OBJ = text_stat.o
CFLAGS = -Wall -Wextra
TARGET=text_stat

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) $^ -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) $^ -o $@

.PHONY: clean

clean:
	rm -f *.o *~ text_stat

.PHONY: install

install:
	cp -f text_stat /usr/bin/text_stat

.PHONY: uninstall

uninstall:
	rm -f /usr/bin/text_stat
