/*
 * text_stat.c
 * 
 * Copyright 2020 Aleksey Melnikov <melnikov@melnikov-PC>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <wchar.h>
#include "text_stat.h"

void output_stats(struct t_stat *stats)
{
	printf("Text Stats:\n");
	printf("	lines: %d\n", stats->lines);
	printf("	words: %d\n", stats->words);
	printf("	letters per word: %.1f\n", stats->lpw);
	printf("	most commmon letter: %c\n", stats->mcl);
}

void calc_stats(struct t_stat *stats)
{
	unsigned int max = 0;
	
	stats->lpw /= stats->words;
	
	for (int i = 0; i < ASCII_LEN; i++) {
		if (stats->freq_arr[i] > max) {
			max = stats->freq_arr[i];
			stats->mcl = i;
		}
	}
}

void process_line(char *line, struct t_stat *stats)
{
	char *word;
	wchar_t  ws[WS_BUF];
	
	word = strtok(line, WORD_DELIM);
	while (word) {
		//this thing below drop words with accidental non ASCII characters,
		//because of SEGFAULT on (unsigned int) cast
		swprintf(ws, WS_BUF, L"%hs", word);
		if (wcslen(ws)) {
			for (unsigned int i = 0; i < strlen(word); i++)
				stats->freq_arr[(unsigned int)word[i]]++;
			
			stats->lpw += strlen(word);
			stats->words++;
		}

		word = strtok(NULL, WORD_DELIM);
	}
}

char* get_file_name(int argc, char **argv)
{
	if (argc != 2) {
		printf("No valid path to text or wrong arguments, using default instead!\n");
		return DEF_TEXT;
	} else if (!strcmp(argv[1], "help")) {
		printf("Usage: 'text_stat <path_to_file>'\n");
		exit(0);
	} else
		return argv[1];
}

int main(int argc, char **argv)
{
	char *file_name;
	char *line;
	size_t line_len;
	FILE *fp;
	
	file_name = get_file_name(argc, argv);
	
	printf("Opening file '%s'\n", file_name);
	
	fp = fopen(file_name, "r");
	if (!fp) {
		perror(NULL);
		return -1;
	}
	
	struct t_stat *text_stats = (struct t_stat *)malloc(sizeof(struct t_stat));
	memset(text_stats, 0, sizeof(struct t_stat));
	while (getline(&line, &line_len, fp) != -1) {
		text_stats->lines++;
		process_line(line, text_stats);
	}
	
	calc_stats(text_stats);
	output_stats(text_stats);
	
	free(line);			//malloc inside getline()
	free(text_stats);
	fclose(fp);
	
	return 0;
}
