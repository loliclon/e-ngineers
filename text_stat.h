#define DEF_TEXT	"default_text"
#define WORD_DELIM	" ,.;:(){}?!\n\r"	//i guess we have other delimeters besides spaces
#define ASCII_LEN	255
#define WS_BUF		512

struct t_stat{
	unsigned int words;
	unsigned int lines;
	float lpw;							//letters per word
	unsigned int freq_arr[ASCII_LEN];	//usage frequency by ascii code
	unsigned int mcl;					//most common letter
};
